extends KinematicBody2D

const up = Vector2(0,-1)

onready var skull = load("res://prefabs/Skull.tscn")

var dir = Vector2(200,1000)
var active:bool = false

# warning-ignore:unused_argument
func _physics_process(delta):
	if(active):
# warning-ignore:return_value_discarded
		move_and_slide(dir,up)


# warning-ignore:unused_argument
func _on_DeathZone_body_entered(body):
	var SkullInstace = skull.instance()
	SkullInstace.set_position(Vector2(self.position.x, self.position.y - 100))
	get_parent().add_child(SkullInstace)
	self.queue_free()
