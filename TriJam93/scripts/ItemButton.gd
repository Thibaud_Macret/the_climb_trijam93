extends Button

export var nbLeft:int = 0
export var id:int

var is_pressed:bool = false

func _ready():
	actualise()

func actualise():
	$NbLeft.text = String(nbLeft)
	if (nbLeft == 0):
		$NbLeft.set("custom_colors/font_color", Color("FF0000"))

func _on_PlankButton_pressed():
	is_pressed = !is_pressed
	$"../Grid".visible = is_pressed
	if(is_pressed):
		$"..".placing_block = id
	else:
		$"..".placing_block = 0
