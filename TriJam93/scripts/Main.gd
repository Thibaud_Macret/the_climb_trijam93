extends Node2D

onready var plank = load("res://prefabs/Planks.tscn")
onready var bumper = load("res://prefabs/Bumper.tscn")
onready var rock = load("res://prefabs/FragileRock.tscn")

export var next_level_number:int

var placing_block:int = 0

# warning-ignore:unused_argument
func _physics_process(delta):
	if(Input.is_action_just_pressed("Click")):
		if(get_global_mouse_position().y >=0):
			place_block()

func place_block():
	match placing_block:
		1:
			if($PlankButton.nbLeft != 0):
				var Instance = plank.instance()
				Instance.set_position(round_coord(get_global_mouse_position()))
				self.add_child(Instance)
				$PlankButton.nbLeft -= 1
				$PlankButton.actualise()
		2:
			if($BumperButton.nbLeft != 0):
				var Instance = bumper.instance()
				Instance.set_position(round_coord(get_global_mouse_position()))
				self.add_child(Instance)
				$BumperButton.nbLeft -= 1
				$BumperButton.actualise()
		3:
			if($FragileRockButton.nbLeft != 0):
				var Instance = rock.instance()
				Instance.set_position(round_coord(get_global_mouse_position()))
				self.add_child(Instance)
				$FragileRockButton.nbLeft -= 1
				$FragileRockButton.actualise()

func round_coord(coord:Vector2) ->Vector2 :
	var temp_return = Vector2()
	temp_return.x = int(coord.x/64) * 64 + 32
	temp_return.y = int(coord.y/64) * 64 + 32
	return temp_return


# warning-ignore:unused_argument
func _on_Goal_body_entered(body):
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Level" + String(next_level_number) + ".tscn")
