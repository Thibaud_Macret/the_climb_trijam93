extends Sprite

var living_time:float = .5

func _physics_process(delta):
	living_time -= delta
	if(living_time < 0):
		self.queue_free()